import React from 'react';
import {
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const Home = ({navigation}) => {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <View style={styles.kelasContainer}>
        <View style={styles.containerTitle}>
          <Text style={{color: 'white', paddingLeft: 10}}>Kelas</Text>
        </View>
        <View style={styles.kelasContent}>
          <View style={styles.contentIcon}>
            <TouchableOpacity onPress={() => navigation.navigate('Chart')}>
              <Icon name="logo-react" size={45} color={'white'} />
              <Text style={styles.iconDescription}>React Native</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.contentIcon}>
            <Icon name="logo-python" size={45} color={'white'} />
            <Text style={styles.iconDescription}>Data Science</Text>
          </View>
          <View style={styles.contentIcon}>
            <Icon name="logo-react" size={45} color={'white'} />
            <Text style={styles.iconDescription}>React JS</Text>
          </View>
          <View style={styles.contentIcon}>
            <Icon name="logo-laravel" size={45} color={'white'} />
            <Text style={styles.iconDescription}>Laravel</Text>
          </View>
        </View>
      </View>
      <View style={styles.kelasContainer}>
        <View style={styles.containerTitle}>
          <Text style={{color: 'white', paddingLeft: 10}}>Kelas</Text>
        </View>
        <View style={styles.kelasContent}>
          <View style={styles.contentIcon}>
            <Icon name="logo-wordpress" size={45} color={'white'} />
            <Text style={styles.iconDescription}>Wordpress</Text>
          </View>
          <View style={styles.contentIcon}>
            <Image
              style={{width: 45, height: 45}}
              source={require('../../assets/icons/website-design.png')}
            />
            <Text style={styles.iconDescription}>Design Grafis</Text>
          </View>
          <View style={styles.contentIcon}>
            <MaterialIcon name="server" size={45} color={'white'} />
            <Text style={styles.iconDescription}>Web Server</Text>
          </View>
          <View style={styles.contentIcon}>
            <Image
              style={{width: 45, height: 45}}
              source={require('../../assets/icons/ux.png')}
            />
            <Text style={styles.iconDescription}>UI/UX Design</Text>
          </View>
        </View>
      </View>
      <View style={styles.summaryContainer}>
        <View style={styles.containerTitle}>
          <Text style={{color: 'white', paddingLeft: 10}}>Summary</Text>
        </View>
        <ScrollView style={styles.scrollView}>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>React Native</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>Data Science</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>React JS</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>Laravel</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>Wordpress</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>Design Grafis</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>Web Server</Text>
          </View>
          <View style={styles.summaryContent}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
          <View style={styles.summaryTitle}>
            <Text style={{color: 'white', paddingLeft: 10}}>UI/UX Design</Text>
          </View>
          <View
            style={{
              display: 'flex',
              backgroundColor: '#088dc4',
              paddingVertical: 5,
              borderBottomRightRadius: 10,
              borderBottomLeftRadius: 10,
            }}>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Today</Text>
              <Text style={styles.summaryTextContent}>20 orang</Text>
            </View>
            <View style={styles.summaryRowContent}>
              <Text style={styles.summaryTextContent}>Total</Text>
              <Text style={styles.summaryTextContent}>100 orang</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

export default Home;
