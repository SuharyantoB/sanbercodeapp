import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loginContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: '#036',
    fontSize: 26,
    fontWeight: 'bold',
  },
  image: {
    width: Dimensions.get('window').width * 0.5,
    height: Dimensions.get('window').width * 0.5,
    paddingVertical: 90,
  },
  text: {
    color: '#a4a4a6',
    fontWeight: 'bold',
    fontSize: 16,
    textAlign: 'center',
  },
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: '#191970',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textName: {
    fontSize: 24,
    color: '#003366',
    fontWeight: 'bold',
    padding: 4,
  },
  formContainer: {
    paddingHorizontal: 30,
    marginBottom: 10,
  },
  input: {
    padding: 10,
    height: 40,
  },
  buttonContainer: {
    marginTop: 30,
    height: 130,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnLight: {
    height: 40,
    backgroundColor: '#3EC6FF',
    width: Dimensions.get('window').width * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  btnDark: {
    height: 40,
    backgroundColor: '#d23f31',
    width: Dimensions.get('window').width * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  btnGoogle: {
    height: 45,
    width: Dimensions.get('window').width * 0.87,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  btnFinger: {
    marginTop: 10,
    height: 40,
    backgroundColor: '#191970',
    width: Dimensions.get('window').width * 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  regContainer: {
    marginTop: 50,
    flexDirection: 'row',
    marginHorizontal: 24,
    justifyContent: 'center',
  },
});

export default styles;
