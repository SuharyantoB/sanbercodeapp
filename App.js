import React, {useEffect} from 'react';
import firebase from '@react-native-firebase/app';
// import Intro from './src/screens/Tugas1/Intro';
// import Profile from './src/screens/Tugas2/Profile';
// import TodoList from './src/screens/Tugas3/TodoList';
// import Context from './src/screens/Tugas4/index';
// import Splashscreen from './src/navigation/navigation';
// import AuthenticationJWT from './src/navigation/navigation';
// import AuthenticationGoogle from './src/navigation/navigation';
// import AuthenticationFinger from './src/navigation/navigation';
// import Home from './src/screens/Home/Home';
// import Chart from './src/screens/Chart/Chart';
import Main from './src/navigation/navigation';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';
import {Alert} from 'react-native';

// code-push release-react SanbercodeApp android
// code-push deployment list SanbercodeApp
// code-push promote SanbercodeApp Staging Production

//ganti isi dari firebaseConfig, sesuai dengan project yang anda buat di firebase
var firebaseConfig = {
  apiKey: 'AIzaSyBw6Apm-9TFB1oqgZjQwhX0zqHtvzRnDsE',
  authDomain: 'sanbercode-3b002.firebaseapp.com',
  databaseURL: 'https://sanbercode-3b002.firebaseio.com',
  projectId: 'sanbercode-3b002',
  storageBucket: 'sanbercode-3b002.appspot.com',
  messagingSenderId: '600069288543',
  appId: '1:600069288543:web:72261ff564e3e7446421ea',
  measurementId: 'G-XBRKBPTTW3',
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init('a347dba3-f0ea-4e0f-9bc4-d4ec76ca5f81', {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 2,
    });
    OneSignal.inFocusDisplaying(2);

    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);

    codePush.sync(
      {
        updateDialog: true,
        installMode: codePush.InstallMode.IMMEDIATE,
      },
      SyncStatus,
    );

    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  }, []);

  const SyncStatus = (status) => {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking Update');
        break;

      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        console.log('Downloading Package');
        break;

      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to date');
        break;

      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('Installing Update');
        break;

      case codePush.SyncStatus.UPDATE_INSTALLED:
        Alert.alert('Notification', 'Update Installed');
        break;

      case codePush.SyncStatus.AWAITING_USER_ACTION:
        console.log('Awaiting user action');
        break;
      default:
        break;
    }
  };

  const onReceived = (notification) => {
    console.log('onReceived -> notification', notification);
  };

  const onOpened = (openResult) => {
    console.log('onOpened -> openResult', openResult);
  };

  const onIds = (device) => {
    console.log('onIds -> device', device);
  };

  return <Main />;
};

export default App;
