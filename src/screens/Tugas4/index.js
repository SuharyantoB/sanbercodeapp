import React, {useState, createContext} from 'react';
import TodoList from './TodoList';

export const RootContext = createContext();

const Context = () => {
  const [list, setList] = useState([]);
  const [textInput, setTextInput] = useState('');

  handleChangeInput = (text) => {
    setTextInput(text);
  };

  addList = () => {
    if (textInput) {
      let d = new Date();
      let strDate =
        d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();

      setList([...list, {date: strDate, text: textInput}]);
      setTextInput('');
    }
  };

  delList = (index) => {
    let newlist = [...list];
    newlist.splice(index, 1);
    setList(newlist);
  };

  return (
    <RootContext.Provider
      value={{
        list,
        textInput,
        handleChangeInput,
        addList,
        delList,
      }}>
      <TodoList />
    </RootContext.Provider>
  );
};
export default Context;
