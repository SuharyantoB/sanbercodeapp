import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

const {height, width} = Dimensions.get('window');

const Profile = ({navigation}) => {
  const [userInfo, setUserInfo] = useState(null);
  const [userToken, setUserToken] = useState(null);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('token');
        setUserToken(token);
        // console.log('getToken -> token', token);
        // return getVenue(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
    getCurrentUser();
  }, [userInfo]);

  const getCurrentUser = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      // console.log('getCurrentUser->userInfo', userInfo);
      setUserInfo(userInfo);
    } catch (error) {
      console.log(error);
    }
  };

  const getVenue = (token) => {
    Axios.get(`${api}/venues`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer' + token,
      },
    })
      .then((res) => {
        console.log('profile -> res', res);
      })
      .catch((err) => {
        console.log('profile -> err', err);
      });
  };

  const onLogoutPress = async () => {
    try {
      // await GoogleSignin.revokeAccess();
      // await GoogleSignin.signOut();
      // await AsyncStorage.removeItem('token');
      // navigation.navigate('Login');
      if (userInfo != null) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
      if (userToken != null) {
        await AsyncStorage.removeItem('token');
      }
      navigation.navigate('Login');
      await AsyncStorage.removeItem('skipLogin');
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
      <View style={styles.boxUpper}>
        <Image
          source={
            userInfo === null
              ? require('../../assets/images/profile.jpg')
              : {uri: userInfo && userInfo.user && userInfo.user.photo}
          }
          style={{width: 80, height: 80, borderRadius: 40, marginBottom: 15}}
        />
        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>
          {userInfo === null
            ? `Suharyanto`
            : `${userInfo && userInfo.user && userInfo.user.name}`}
        </Text>
      </View>
      <View style={styles.boxUnder}>
        <View style={styles.boxRound}>
          <View style={styles.boxData}>
            <View>
              <Text style={styles.textDetail}>Tanggal Lahir</Text>
              <Text style={styles.textDetail}>Jenis Kelamin</Text>
              <Text style={styles.textDetail}>Hobi</Text>
              <Text style={styles.textDetail}>No. Telp</Text>
              <Text style={styles.textDetail}>Email</Text>
            </View>
            <View style={{alignItems: 'flex-end'}}>
              <Text style={styles.textDetail}>09 Juni 1987</Text>
              <Text style={styles.textDetail}>Laki - laki</Text>
              <Text style={styles.textDetail}>Tidur</Text>
              <Text style={styles.textDetail}>081992244800</Text>
              <Text style={styles.textDetail}>
                {userInfo === null
                  ? `yanto512@gmail.com`
                  : `${userInfo && userInfo.user && userInfo.user.email}`}
              </Text>
            </View>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => onLogoutPress()}
              style={styles.buttonLogout}>
              <Text
                style={{
                  backgroundColor: '#3EC6FF',
                  color: 'white',
                  height: 40,
                  paddingHorizontal: 25,
                  paddingVertical: 5,
                  borderRadius: 5,
                  width: width * 0.9,
                  textAlign: 'center',
                  fontSize: 20,
                }}>
                LOGOUT
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
  },
  boxUpper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF',
    height: height * 0.33,
  },
  boxUnder: {
    backgroundColor: 'white',
  },
  boxData: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  boxRound: {
    backgroundColor: 'white',
    borderRadius: 8,
    elevation: 5,
    marginTop: -40,
    width: width * 0.95,
    alignSelf: 'center',
  },
  textDetail: {
    fontSize: 13,
    marginVertical: 7,
  },
  buttonLogout: {
    marginTop: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
});

export default Profile;
