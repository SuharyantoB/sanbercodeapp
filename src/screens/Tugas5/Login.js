import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Button,
  TextInput,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import styles from '../../style/style';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import TouchID from 'react-native-touch-id';

const config = {
  title: 'Authentication Required',
  imageColor: '#191970',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const saveToken = async (token) => {
    try {
      await AsyncStorage.setItem('token', token);
    } catch (err) {
      console.log(err);
    }
  };

  const onLogin = async () => {
    await AsyncStorage.setItem('skipLogin', 'true');
  };

  useEffect(() => {
    configureGoogleSignIn();
  }, []);

  const configureGoogleSignIn = () => {
    GoogleSignin.configure({
      offlineAccess: false,
      webClientId:
        '600069288543-2lefm5s6mtriu3big3upl5tkm9os4kfb.apps.googleusercontent.com',
    });
  };

  const signInWithGoogle = async () => {
    try {
      const {idToken} = await GoogleSignin.signIn();
      console.log('SignInWithGoogle -> idToken', idToken);
      const credential = auth.GoogleAuthProvider.credential(idToken);
      auth().signInWithCredential(credential);
      onLogin();
      navigation.navigate('Profile');
    } catch (error) {
      console.log('SignInWithGoogle -> error', error);
    }
  };

  const onLoginProcess = () => {
    return auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {
        // navigation.navigate('Profile');
        setEmail('');
        setPassword('');
        onLogin();
        navigation.reset({
          index: 0,
          routes: [{name: 'Profile'}],
        });
      })
      .catch((error) => {
        console.log('onLoginPress -> error', error);
        alert(error);
      });
  };

  // const onLoginProcess = () => {
  //   let data = {
  //     email: email,
  //     password: password,
  //   };
  //   Axios.post(`${api}/login`, data, {
  //     timeout: 20000,
  //   })
  //     .then((res) => {
  //       console.log('login -> res', res);
  //       saveToken(res.data.token);
  //       navigation.navigate('Profile');
  //       setEmail('');
  //       setPassword('');
  //     })
  //     .catch((err) => {
  //       console.log('login -> err', err);
  //       alert('Wrong Username or Password');
  //     });
  // };

  const signInWithFingerprint = () => {
    TouchID.authenticate('', config)
      .then((success) => {
        alert('Authentication Success');
        navigation.navigate('Profile');
      })
      .catch((error) => {
        alert('Authentication Failed');
      });
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <View style={{paddingTop: 10}}>
        <View style={{marginTop: 50, alignItems: 'center'}}>
          <Image
            source={require('../../assets/images/logo.jpg')}
            style={{height: 150, width: 300}}
          />
        </View>
        {/* <View style={{alignItems: 'center', padding: 20}}>
          <Text style={[styles.textName, styles.label]}>Login</Text>
        </View> */}
        <View style={{paddingTop: 20}}>
          <View style={styles.formContainer}>
            <Text style={styles.label}>Username / Email</Text>
            <TextInput
              style={styles.input}
              value={email}
              underlineColorAndroid="#c6c6c6"
              placeholder="Username or Email"
              onChangeText={(email) => setEmail(email)}
            />
          </View>
          <View style={styles.formContainer}>
            <Text style={styles.label}>Password</Text>
            <TextInput
              style={styles.input}
              secureTextEntry
              value={password}
              underlineColorAndroid="#c6c6c6"
              placeholder="Password"
              onChangeText={(password) => setPassword(password)}
            />
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.btnLight}
              onPress={() => onLoginProcess()}>
              <Text style={{color: 'white', fontSize: 16}}>LOGIN</Text>
            </TouchableOpacity>

            <Text style={{color: 'black', fontSize: 16}}>OR</Text>
            {/* <TouchableOpacity style={styles.btnDark}>
              <Text style={{color: 'white', fontSize: 16}}>
                LOGIN WITH GOOGLE
              </Text>
            </TouchableOpacity> */}
            <GoogleSigninButton
              onPress={() => signInWithGoogle()}
              style={styles.btnGoogle}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
            />
            <TouchableOpacity
              style={styles.btnFinger}
              onPress={() => signInWithFingerprint()}>
              <Text style={{color: 'white', fontSize: 16}}>
                SIGN IN WITH FINGERPRINT
              </Text>
            </TouchableOpacity>
            <View style={styles.regContainer}>
              <Text>Belum mempunyai akun ? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                <Text style={{color: '#3EC6FF'}}>Buat Akun</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Login;
