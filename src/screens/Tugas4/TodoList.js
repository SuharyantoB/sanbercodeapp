import React, {useContext} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {RootContext} from './index';

function TodoList() {
  const state = useContext(RootContext);

  const showList = ({item, index}) => {
    return (
      <View style={styles.list}>
        <View>
          <Text>{item.date}</Text>
          <Text>{item.text}</Text>
        </View>
        <TouchableOpacity
          onPress={() => state.delList(index)}
          style={styles.delButton}>
          <Ionicons name="trash-outline" size={24} color="black" />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={{marginBottom: 10}}>Masukan Todolist </Text>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <TextInput
            value={state.textInput}
            style={styles.textInput}
            onChangeText={(text) => state.handleChangeInput(text)}
            placeholder={'Input here!'}
          />
          <TouchableOpacity
            onPress={() => state.addList()}
            style={styles.addButton}>
            <Ionicons name="add" size={26} color="black" />
          </TouchableOpacity>
        </View>
      </View>
      <View style={{flex: 1, paddingHorizontal: 10}}>
        <FlatList
          data={state.list}
          renderItem={showList}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    marginTop: 10,
    padding: 10,
    height: 100,
  },
  textInput: {
    flex: 1,
    height: 50,
    alignItems: 'stretch',
    borderWidth: 1,
    borderColor: '#cacaca',
    color: '#000',
    paddingHorizontal: 20,
  },
  addButton: {
    height: 50,
    width: 50,
    backgroundColor: '#3ec6ff',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
  },
  delButton: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    top: 10,
    bottom: 10,
    right: 10,
  },
  list: {
    padding: 20,
    paddingRight: 100,
    borderWidth: 5,
    borderRadius: 5,
    borderColor: '#ededed',
    marginBottom: 10,
  },
});

export default TodoList;
