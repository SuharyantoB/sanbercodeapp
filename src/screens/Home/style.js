import {StyleSheet, Dimensions} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
  kelasContainer: {
    display: 'flex',
    width: Dimensions.get('window').width * 0.95,
    marginVertical: 5,
  },
  containerTitle: {
    backgroundColor: '#088dc4',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: 30,
    justifyContent: 'center',
  },
  kelasContent: {
    backgroundColor: '#3EC6FF',
    flexDirection: 'row',
    paddingHorizontal: 10,
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  contentIcon: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconDescription: {
    color: 'white',
    fontSize: 10,
  },
  summaryContainer: {
    display: 'flex',
    width: Dimensions.get('window').width * 0.95,
    height: Dimensions.get('window').height * 0.57,
    marginVertical: 5,
  },
  scrollView: {
    display: 'flex',
  },
  summaryTitle: {
    backgroundColor: '#3EC6FF',
    height: 30,
    justifyContent: 'center',
  },
  summaryContent: {
    display: 'flex',
    backgroundColor: '#088dc4',
    paddingVertical: 5,
  },
  summaryTextContent: {
    color: 'white',
  },
  summaryRowContent: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 30,
    marginVertical: 2,
  },
});

export default styles;
