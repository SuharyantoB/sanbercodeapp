import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  Modal,
  StatusBar,
  TextInput,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';
import {RNCamera} from 'react-native-camera';
import storage from '@react-native-firebase/storage';

const {height, width} = Dimensions.get('window');

const Register = ({navigation}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [type, setType] = useState('back');
  const [photo, setPhoto] = useState(null);

  const toogleCamera = () => {
    setType(type === 'back' ? 'front' : 'back');
  };

  const takePicture = async () => {
    const options = {quality: 0.5, base64: true};
    if (camera) {
      const data = await camera.takePictureAsync(options);
      console.log('takePicture -> data', data);
      setPhoto(data);
      setIsVisible(false);
    }
  };

  const uploadImage = (uri) => {
    const sessionId = new Date().getTime();
    return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert(`Upload Success`);
        navigation.navigate('Login');
      })
      .catch((error) => {
        alert(error);
      });
  };

  const RenderCamera = () => {
    return (
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{flex: 1}}
            ref={(ref) => {
              camera = ref;
            }}
            type={type}>
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => toogleCamera()}>
                <MaterialCommunityIcons name="rotate-3d-variant" size={15} />
              </TouchableOpacity>
            </View>
            <View style={styles.round} />
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity
                style={styles.btnTake}
                onPress={() => takePicture()}>
                <Feather name="camera" size={30} />
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    );
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#3EC6FF" barStyle="dark-content" />
      <View style={styles.boxUpper}>
        <Image
          source={
            photo === null
              ? require('../../assets/images/profile.jpg')
              : {uri: photo.uri}
          }
          style={{width: 80, height: 80, borderRadius: 40, marginBottom: 15}}
        />
        <TouchableOpacity
          onPress={() => {
            setIsVisible(true);
          }}>
          <Text style={{color: 'white', fontWeight: 'bold', fontSize: 18}}>
            Change Picture
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.boxUnder}>
        <View style={styles.boxRound}>
          <View style={styles.boxData}>
            <View>
              <Text style={styles.registerLabel}>Nama</Text>
              <TextInput
                style={styles.registerInput}
                underlineColorAndroid="#c6c6c6"
                placeholder="Name"
              />
            </View>
            <View>
              <Text style={styles.registerLabel}>Email</Text>
              <TextInput
                style={styles.registerInput}
                underlineColorAndroid="#c6c6c6"
                placeholder="Email"
              />
            </View>
            <View>
              <Text style={styles.registerLabel}>Password</Text>
              <TextInput
                secureTextEntry={true}
                style={styles.registerInput}
                underlineColorAndroid="#c6c6c6"
                placeholder="Password"
              />
            </View>
          </View>
          <View>
            <TouchableOpacity
              style={styles.buttonLogout}
              onPress={() => uploadImage(photo.uri)}>
              <Text
                style={{
                  backgroundColor: '#3EC6FF',
                  color: 'white',
                  height: 40,
                  paddingHorizontal: 25,
                  paddingVertical: 5,
                  borderRadius: 5,
                  width: width * 0.85,
                  textAlign: 'center',
                  fontSize: 20,
                }}>
                REGISTER
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <RenderCamera />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    backgroundColor: '#fff',
  },
  boxUpper: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3EC6FF',
    height: height * 0.33,
  },
  boxUnder: {
    backgroundColor: 'white',
  },
  boxData: {
    paddingHorizontal: 15,
    flexDirection: 'column',
    paddingVertical: 10,
  },
  boxRound: {
    backgroundColor: 'white',
    borderRadius: 8,
    elevation: 5,
    marginTop: -40,
    width: width * 0.9,
    alignSelf: 'center',
  },
  textDetail: {
    fontSize: 13,
    marginVertical: 7,
  },
  buttonLogout: {
    marginTop: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
  registerLabel: {
    fontWeight: 'bold',
    marginTop: 10,
  },
  registerInput: {
    padding: 10,
    marginLeft: 5,
  },
  btnFlipContainer: {
    margin: 10,
  },
  btnFlip: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  round: {
    width: height * 0.3,
    height: height * 0.45,
    borderRadius: 100,
    borderWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
  },
  rectangle: {
    marginTop: 50,
    width: width * 0.65,
    height: width * 0.4,
    borderWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
  },
  btnTakeContainer: {
    alignItems: 'center',
  },
  btnTake: {
    marginTop: 20,
    height: 80,
    width: 80,
    borderRadius: 50,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Register;
