import React, {useState} from 'react';
import {Text, View, processColor} from 'react-native';
import styles from './style';
import {BarChart} from 'react-native-charts-wrapper';

const Chart = ({navigation}) => {
  const data = [
    {y: [100, 40], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [80, 60], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [40, 90], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [78, 45], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [67, 87], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [98, 32], marker: ['React Native Dasar', 'React Native Lanjutan']},
    {y: [150, 90], marker: ['React Native Dasar', 'React Native Lanjutan']},
  ];

  const markerData = [];
  data.forEach(function (item, index) {
    const [y0, y1] = item.y;
    const [marker0, marker1] = item.marker;
    markerData[index] = {
      y: [y0, y1],
      marker: [`${marker0}\n${y0}`, `${marker1}\n${y1}`],
    };
  });

  const [legend, setLegend] = useState({
    enabled: true,
    textSize: 14,
    form: 'SQUARE',
    fontSize: 14,
    xEntrySpace: 10,
    yEntrySpace: 5,
    formToTextSpace: 5,
    wordWrapEnabled: true,
    maxSizePercent: 0.5,
  });

  const [chart, setChart] = useState({
    data: {
      dataSets: [
        {
          //   values: [
          //     {y: 100, marker: 'Contoh detail marker'},
          //     {y: 105},
          //     {y: 102},
          //     {y: 110},
          //     {y: 114},
          //     {y: 109},
          //     {y: 105},
          //     {y: 102},
          //     {y: 110},
          //   ],
          //   values: data,
          values: markerData,
          label: '',
          config: {
            colors: [processColor('#3EC6FF'), processColor('#088dc4')],
            stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
            // colors: [processColor('#3EC6FF')],
            // stackLabels: [],
            drawFilled: false,
            drawValues: false,
          },
        },
      ],
    },
  });

  const [xAxis, setXAxis] = useState({
    valueFormatter: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Agu',
      'Sep',
      'Oct',
      'Nov',
      'Des',
    ],
    position: 'BOTTOM',
    drawAxisLine: true,
    drawGridLines: false,
    axisMinimum: -0.5,
    granularityEnabled: true,
    granularity: 1,
    axisMaximum: new Date().getMonth() + 0.5,
    spaceBetweenLabels: 0,
    labelRotationAngle: -45.0,
    limitLines: [{limit: 115, lineColor: processColor('red'), lineWidth: 1}],
  });

  const [yAxis, setYAxis] = useState({
    left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false,
    },
    right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false,
    },
  });

  return (
    <View style={styles.container}>
      <BarChart
        style={{flex: 1}}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        chartDescription={{text: ''}}
        legend={legend}
        marker={{
          enabled: true,
          markerColor: processColor('grey'),
          textColor: processColor('white'),
          textSize: 14,
        }}
      />
    </View>
  );
};

export default Chart;
