import React, {useEffect} from 'react';
import {Text, View} from 'react-native';
import styles from './style';
import MapboxGL from '@react-native-mapbox-gl/maps';

MapboxGL.setAccessToken(
  'pk.eyJ1Ijoic3BlZWR5bGFzdCIsImEiOiJja2V0Z3QxZjAxbWJhMnFvZmtsYW9uZGd0In0.icdB36RFw6bl9BVZY_z79A',
);

const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
];

const Map = ({navigation}) => {
  useEffect(() => {
    const getLocation = async () => {
      try {
        const permission = await MapboxGL.requestAndroidLocationPermissions();
      } catch (error) {
        console.log(error);
      }
    };
    getLocation();
  }, []);

  return (
    <View style={styles.container}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />

        {coordinates.map((koordinat, i) => {
          return (
            <MapboxGL.PointAnnotation
              key={i}
              id={i.toString()}
              coordinate={koordinat}>
              <MapboxGL.Callout
                title={`Longitude: ${koordinat[0]} Latitude: ${koordinat[1]}`}
              />
            </MapboxGL.PointAnnotation>
          );
        })}

        {/* <MapboxGL.PointAnnotation
          id="pointAnnotation"
          coordinate={[107.61074, -6.891356]}>
          <MapboxGL.Callout title="Ini adalah marker" />
        </MapboxGL.PointAnnotation> */}
      </MapboxGL.MapView>
    </View>
  );
};

export default Map;
